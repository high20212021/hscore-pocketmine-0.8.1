<?php

/**
 *
 *  ____            _        _   __  __ _                  __  __ ____  
 * |  _ \ ___   ___| | _____| |_|  \/  (_)_ __   ___      |  \/  |  _ \ 
 * | |_) / _ \ / __| |/ / _ \ __| |\/| | | '_ \ / _ \_____| |\/| | |_) |
 * |  __/ (_) | (__|   <  __/ |_| |  | | | | | |  __/_____| |  | |  __/ 
 * |_|   \___/ \___|_|\_\___|\__|_|  |_|_|_| |_|\___|     |_|  |_|_| 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author PocketMine Team
 * @link http://www.pocketmine.net/
 * 
 *
*/

class AchievementAPI{
	public static $achievements = array(
		/*"openInventory" => array(
			"name" => "Taking Inventory",
			"requires" => array(),
		),*/
		"mineWood" => array(
			"name" => "[获得木头]",
			"requires" => array(
				//"openInventory",
			),
		),
		"buildWorkBench" => array(
			"name" => "[这是工作台！]",
			"requires" => array(
				"mineWood",
			),
		),
		"buildPickaxe" => array(
			"name" => "[挖矿时间到！]",
			"requires" => array(
				"buildWorkBench",
			),
		),
		"buildFurnace" => array(
			"name" => "[热点话题]",
			"requires" => array(
				"buildPickaxe",
			),
		),
		"acquireIron" => array(
			"name" => "[铁锭！]",
			"requires" => array(
				"buildFurnace",
			),
		),
		"buildHoe" => array(
			"name" => "[开垦农田]",
			"requires" => array(
				"buildWorkBench",
			),
		),
		"makeBread" => array(
			"name" => "[烤面包]",
			"requires" => array(
				"buildHoe",
			),
		),
		"bakeCake" => array(
			"name" => "[蛋糕是个谎言]",
			"requires" => array(
				"buildHoe",
			),
		),
		"buildBetterPickaxe" => array(
			"name" => "[获得升级]",
			"requires" => array(
				"buildPickaxe",
			),
		),
		"buildSword" => array(
			"name" => "[我的神剑！]",
			"requires" => array(
				"buildWorkBench",
			),
		),
		"diamonds" => array(
			"name" => "[钻石！]",
			"requires" => array(
				"acquireIron",
			),
		),
		
	);

	function __construct(){
	}
	
	public static function broadcastAchievement(Player $player, $achievementId){
		if(isset(self::$achievements[$achievementId])){
			$result = ServerAPI::request()->api->dhandle("achievement.broadcast", array("player" => $player, "achievementId" => $achievementId));
			if($result !== false and $result !== true){
				if(ServerAPI::request()->api->getProperty("announce-player-achievements") == true){
					ServerAPI::request()->api->chat->broadcast($player->username." 获得了成就 ".self::$achievements[$achievementId]["name"]);
				}else{
					$player->sendChat("你获得了成就 ".self::$achievements[$achievementId]["name"]);
				}			
			}
			return true;
		}
		return false;
	}
	
	public static function addAchievement($achievementId, $achievementName, array $requires = array()){
		if(!isset(self::$achievements[$achievementId])){
			self::$achievements[$achievementId] = array(
				"name" => $achievementName,
				"requires" => $requires,
			);
			return true;
		}
		return false;
	}
	
	public static function hasAchievement(Player $player, $achievementId){
		if(!isset(self::$achievements[$achievementId]) or !isset($player->achievements)){
			$player->achievements = array();
			return false;
		}
		
		if(!isset($player->achievements[$achievementId]) or $player->achievements[$achievementId] == false){
			return false;
		}
		return true;
	}
	
	public static function grantAchievement(Player $player, $achievementId){
		if(isset(self::$achievements[$achievementId]) and !self::hasAchievement($player, $achievementId)){
			foreach(self::$achievements[$achievementId]["requires"] as $requerimentId){
				if(!self::hasAchievement($player, $requerimentId)){
					return false;
				}
			}
			if(ServerAPI::request()->api->dhandle("achievement.grant", array("player" => $player, "achievementId" => $achievementId)) !== false){
				$player->achievements[$achievementId] = true;
				self::broadcastAchievement($player, $achievementId);
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	public static function removeAchievement(Player $player, $achievementId){
		if(self::hasAchievement($player, $achievementId)){
			$player->achievements[$achievementId] = false;
		}
	}
	
	public function init(){
	}
}
